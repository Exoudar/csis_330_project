///scr_AI_pathing()
/// Pathing and multiplying (currently working on)
while(instance_exists(obj_AI_path)){
    // LEFT AND RIGHT
    if facing == 'right' && !is_falling && !is_climbingUp && !is_climbingDown {
        // First check if we reached this place before
        if place_meeting(x,y,obj_path_pos){
            var pos = instance_place(x,y,obj_path_pos);
            with (pos){
                instance_destroy();
            }
        } else {
            instance_destroy();
            exit;
        }
        if !place_meeting(x+1,y,obj_wall) && (place_meeting(x,y+1,obj_solid) ||  place_meeting(x,y+1,obj_ladder)) && !place_meeting(x,y+1,obj_player){     
            // Testing to go down when a ladder exists
            if place_meeting(x,y+1,obj_ladder) && !place_meeting(x,y+1,obj_player) && !place_meeting(x,y+1,obj_wall){
                var path = instance_create(x,y+32,obj_AI_path);
                path.nextMove = nextMove;
                path.facing = "down";
                path.is_climbingDown = true;
                path.grandFather = grandFather
            }
            
            // Testing to go up when a ladder exists
            if place_meeting(x,y,obj_ladder) && !place_meeting(x,y-1,obj_player) && !place_meeting(x,y-1,obj_wall){
                var path = instance_create(x,y-32,obj_AI_path);
                path.nextMove = nextMove;
                path.facing = "up";
                path.is_climbingUp = true;
                path.grandFather = grandFather
            }
            x += 32;
        } else {
            if place_meeting(x,y,obj_player){
                var move = nextMove;
                with(grandFather){
                    state = move;
                    is_moving = true;
                    finding_player = true;
                    scr_mapping_path();
                }
                with (obj_AI_path){
                    instance_destroy();
                }
                exit;
            }
            y += 32;
            
            // Testing to go left
            if place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_player) && !place_meeting(x-1,y,obj_wall){
                var path = instance_create(x-32,y,obj_AI_path);
                path.nextMove = nextMove;
                path.facing = "left";
                path.grandFather = grandFather;
            }
        }
        if place_meeting(x,y,obj_player){
            var move = nextMove;
            with(grandFather){
                state = move;
                is_moving = true;
                finding_player = true;
                scr_mapping_path();
            }
            with (obj_AI_path){
                instance_destroy();
            }
            exit;
        }
    } else if facing == 'left' && !is_falling && !is_climbingUp && !is_climbingDown{
        // First check if we reached this place before
        if place_meeting(x,y,obj_path_pos){
            var pos = instance_place(x,y,obj_path_pos);
            with (pos){
                instance_destroy();
            }
        } else {
            instance_destroy();
            exit;
        }
        if !place_meeting(x-1,y,obj_wall) && (place_meeting(x,y+1,obj_solid) ||  place_meeting(x,y+1,obj_ladder)) && !place_meeting(x,y+1,obj_player){
            // Testing to go down when a ladder exists
            if place_meeting(x,y+1,obj_ladder) && !place_meeting(x,y+1,obj_player) && !place_meeting(x,y+1,obj_wall){
                var path = instance_create(x,y+32,obj_AI_path);
                path.nextMove = nextMove;
                path.facing = "down";
                path.is_climbingDown = true;
                path.grandFather = grandFather;
            }
            
            // Testing to go up when a ladder exists
            if place_meeting(x,y,obj_ladder) && !place_meeting(x,y-1,obj_player) && !place_meeting(x,y-1,obj_wall){
                var path = instance_create(x,y-32,obj_AI_path);
                path.nextMove = nextMove;
                path.facing = "up";
                path.is_climbingUp = true;
                path.grandFather = grandFather;
            }
            x -= 32;
        } else {
            if place_meeting(x,y,obj_player){
                if place_meeting(x,y,obj_player){
                    var move = nextMove;
                    with(grandFather){
                        state = move;
                        is_moving = true;
                        finding_player = true;
                        scr_mapping_path();
                    }
                    with (obj_AI_path){
                        instance_destroy();
                    }
                    exit;
                }
            }
            y += 32;
            
            // Testing to go right
            if place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_player) && !place_meeting(x+1,y,obj_wall){
                var path = instance_create(x+32,y,obj_AI_path);
                path.nextMove = nextMove;
                path.facing = "right";
                path.grandFather = grandFather;
            }
        }
        if place_meeting(x,y,obj_player){
            var move = nextMove;
            with(grandFather){
                state = move;
                is_moving = true;
                finding_player = true;
                scr_mapping_path();
            }
            with (obj_AI_path){
                instance_destroy();
            }
            exit;
        }
    }
    
    
    // CLIMBING up
    if is_climbingUp == true && is_climbingDown == false && facing == 'up' {
        // First check if we reached this place before
        if place_meeting(x,y,obj_path_pos){
            var pos = instance_place(x,y,obj_path_pos);
            with (pos){
                instance_destroy();
            }
        } else {
            instance_destroy();
            exit;
        }
        if !place_meeting(x,y-1,obj_player) && !place_meeting(x,y-1,obj_wall) && place_meeting(x,y-1,obj_ladder){
            if place_meeting(x,y,obj_player){
                var move = nextMove;
                with(grandFather){
                    state = move;
                    is_moving = true;
                    finding_player = true;
                    scr_mapping_path();
                }
                with (obj_AI_path){
                    instance_destroy();
                }
                exit;
            }
            y -= 32;
            
            if !place_meeting(x,y,obj_ladder){
                // Testing to go left
                if !place_meeting(x-1,y,obj_wall){
                    var path = instance_create(x-32,y,obj_AI_path);
                    path.nextMove = nextMove;
                    path.facing = "left";
                    path.grandFather = grandFather;
                    if place_meeting(x,y,obj_path_pos){
                        var pos = instance_place(x,y,obj_path_pos);
                        with (pos){
                            instance_destroy();
                        }
                    } else {
                        instance_destroy();
                        exit;
                    }
                }
                // Testing to go right
                if !place_meeting(x+1,y,obj_wall){
                    var path = instance_create(x+32,y,obj_AI_path);
                    path.nextMove = nextMove;
                    path.facing = "right";
                    path.grandFather = grandFather;
                    if place_meeting(x,y,obj_path_pos){
                        var pos = instance_place(x,y,obj_path_pos);
                        with (pos){
                            instance_destroy();
                        }
                    } else {
                        instance_destroy();
                        exit;
                    }
                } 
                instance_destroy();
            } else {
                // Testing to go right
                if !place_meeting(x+1,y,obj_wall){
                    var path = instance_create(x+32,y,obj_AI_path);
                    path.nextMove = nextMove;
                    path.facing = "right";
                    path.grandFather = grandFather;
                }
                // Testing to go left
                if !place_meeting(x-1,y,obj_wall){
                    var path = instance_create(x-32,y,obj_AI_path);
                    path.nextMove = nextMove;
                    path.facing = "left";
                    path.grandFather = grandFather;
                }
            }
        } else {
            // Testing to go right
            if !place_meeting(x+1,y,obj_wall){
                var path = instance_create(x+32,y,obj_AI_path);
                path.nextMove = nextMove;
                path.facing = "right";
                path.grandFather = grandFather;
            }
            // Testing to go left
            if !place_meeting(x-1,y,obj_wall){
                var path = instance_create(x-32,y,obj_AI_path);
                path.nextMove = nextMove;
                path.facing = "left";
                path.grandFather = grandFather;
            }
            if place_meeting(x,y-1,obj_player){
                var move = nextMove;
                with(grandFather){
                    state = move;
                    is_moving = true;
                    finding_player = true;
                    scr_mapping_path();
                }
                with (obj_AI_path){
                    instance_destroy();
                }
                exit;
            }
        }
    }
    
    // CLIMBING down
    if is_climbingDown == true && is_climbingUp == false && facing == 'down' {
        if place_meeting(x,y+1,obj_player){
            var move = nextMove;
            with(grandFather){
                state = move;
                is_moving = true;
                finding_player = true;
                scr_mapping_path();
            }
            with (obj_AI_path){
                instance_destroy();
            }
            exit;
        }
        // First check if we reached this place before
        if place_meeting(x,y,obj_path_pos){
            var pos = instance_place(x,y,obj_path_pos);
            with (pos){
                instance_destroy();
            }
        } else {
            instance_destroy();
            exit;
        }
        if !place_meeting(x,y+1,obj_player) && !place_meeting(x,y+1,obj_wall) && place_meeting(x,y+1,obj_ladder){
            if place_meeting(x,y,obj_player){
                var move = nextMove;
                with(grandFather){
                    state = move;
                    is_moving = true;
                    finding_player = true;
                    scr_mapping_path();
                }
                with (obj_AI_path){
                    instance_destroy();
                }
                exit;
            }
            y += 32;
            
            if place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_player){
                // Testing to go left
                if !place_meeting(x-1,y,obj_wall){
                    var path = instance_create(x-32,y,obj_AI_path);
                    path.nextMove = nextMove;
                    path.facing = "left";
                    path.grandFather = grandFather;
                    if place_meeting(x,y,obj_path_pos){
                        var pos = instance_place(x,y,obj_path_pos);
                        with (pos){
                            instance_destroy();
                        }
                    } else {
                        instance_destroy();
                        exit;
                    }
                }
                // Testing to go right
                if !place_meeting(x+1,y,obj_wall){
                    var path = instance_create(x+32,y,obj_AI_path);
                    path.nextMove = nextMove;
                    path.facing = "right";
                    path.grandFather = grandFather;
                    if place_meeting(x,y,obj_path_pos){
                        var pos = instance_place(x,y,obj_path_pos);
                        with (pos){
                            instance_destroy();
                        }
                    } else {
                        instance_destroy();
                        exit;
                    }
                } 
                instance_destroy();
            } else {
                // Testing to go right
                if !place_meeting(x+1,y,obj_wall){
                    var path = instance_create(x+32,y,obj_AI_path);
                    path.nextMove = nextMove;
                    path.facing = "right";
                    path.grandFather = grandFather;
                }
                // Testing to go left
                if !place_meeting(x-1,y,obj_wall){
                    var path = instance_create(x-32,y,obj_AI_path);
                    path.nextMove = nextMove;
                    path.facing = "left";
                    path.grandFather = grandFather;
                }
                if place_meeting(x,y+1,obj_player){
                    var move = nextMove;
                    with(grandFather){
                        state = move;
                        is_moving = true;
                        finding_player = true;
                        scr_mapping_path();
                    }
                    with (obj_AI_path){
                        instance_destroy();
                    }
                    exit;
                }
            }
        }
    }
}
