///scr_moving_robot();

switch (state) {
// ************** MOVING LEFT **************
    case 'moving_left':
        // I will explain for left only. Right should be the same
        
        // Reserving left
        
        // First make sure nothing is on left
        if !place_meeting(x-1,y,obj_solid){
            // Now check if another object already occpies the space left to us
            if place_meeting(x-1,y,obj_occupy_R){
                state = 'hold_L';
                is_moving = false;
                movement_tracking = max_movement_per_command;
            } else {
            // If the space left to us is free, occupy it
                if occupy_reserved == false {
                    occupy_reserved = true;
                    var occupy = instance_create(x-32,y,obj_occupy_L);
                    var occupy_timer = self_speed;
                    var occupy_time_to_destroy = movement_tracking - self_speed;
                    var occupy_free = true;
                    with (occupy) {
                        time_to_destroy = occupy_time_to_destroy;
                        timer = occupy_timer;
                        
                        if place_meeting(x,y,obj_occupy_R){
                            occupy_free = false;
                        }
                    }
                }
            }
        } else {
            // If an object is left of us, then we should stay idle
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command;
            occupy_reserved = false;
        }
        
        // If we are moving left, then our x goes to the left based on our self_speed (-= self_speed)
        if !place_meeting(x-1,y,obj_solid) && is_moving {
            // I tried to make this kinda Turn Based (when you are moving left, you can do nothing untill you go back to idle)
            movement_tracking -= self_speed;
            x -= self_speed;
        } else {
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command;
            occupy_reserved = false;
            self_speed = original_self_speed;
        }
        if movement_tracking <= 0 {
            previous_state = 'moving_left';
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command;
            occupy_reserved = false;
            // returning self speed to normla in case another slower objects pushed it slowly (like robots)
            self_speed = original_self_speed;
        }
        break;
// ************** PUSHING LEFT **************
    case 'pushing_left': 
        // Here we define "movable" as the movable object left of us
        // then we also ONLY allow it to move if nothing is blocking its path.
        var movable = instance_place(x-1,y,obj_movable);
        // We assume we can move, unless something blocked our path
        var can_move = true; 
        // with: a command to control another (example: from player we move a movable next to it)
        var mySpeed = self_speed;
        with (movable) {
            // We make sure that movable (the object we are pushing) has nothing blocking its path (wall or another movable)
            if !place_meeting(x-1,y,obj_solid){
                if !place_meeting(x-1,y,obj_occupy_father) && !place_meeting(x-1,y,obj_spring){
                    // we make sure this happens once, so we dont counter a problem (moving indefinitely)
                    if state != 'moving_left' && state != 'rolling_left' && state != 'falling'{
                        state = 'moving_left';
                        is_moving = true;
                        previous_state = 'idle';
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                        self_speed = mySpeed;
                    }
                } else {
                    can_move = false;
                }      
            } else {
                // If there IS something blocking its path, then we are not allowed to move
                can_move = false;
            }
        }
        // In case we are pushing (a movable object is left of us)
        // we move left similar to moving_left, except now we also push the movable object left with us
        if (can_move){
            if !place_meeting(x-1,y,obj_solid) && is_moving {
                x -= self_speed;
                movement_tracking -= self_speed;
            }
            if movement_tracking <= 0 {
                previous_state = 'pushing_left';
                state = 'idle';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
        } else {
            previous_state = 'pushing_left';
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command;
            occupy_reserved = false;
        }
        break;
// ************** SLIDING LEFT **************
    case 'sliding_left':
        // Reserving left
        
        // First make sure nothing is on left
        if !place_meeting(x-1,y,obj_solid){
            // Now check if another object already occpies the space left to us
            if place_meeting(x-1,y,obj_occupy_R){
                state = 'hold_L';
                is_moving = false;
                movement_tracking = max_movement_per_command;
            } else {
            // If the space left to us is free, occupy it
                if occupy_reserved == false {
                    occupy_reserved = true;
                    var occupy = instance_create(x-32,y,obj_occupy_L);
                    var occupy_timer = self_speed;
                    var occupy_time_to_destroy = movement_tracking - self_speed;
                    var occupy_free = true;
                    with (occupy) {
                        time_to_destroy = occupy_time_to_destroy;
                        timer = occupy_timer;
                        
                        if place_meeting(x,y,obj_occupy_R){
                            occupy_free = false;
                        }
                    }
                }
            }           
        }
        
        if place_meeting(x-1,y,obj_movable){
            // if the object is movable, look at its state
            var movable = instance_place(x-1,y,obj_movable); // defining "movable" as the movable object left of us
            
            // Based on movable's states, we either can slide with it, or get stuck if movable can no longer moves
            if movable.state == 'idle' || movable.state == 'stuck'{
                state = 'stuck';
                is_moving = false;
                previous_state = 'sliding_left';
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            } else if movable.state == 'hold' {
                state = 'hold';
                is_moving = false;
                previous_state = 'sliding_left';
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            } else if movable.state == 'hold_R'{
                state = 'sliding_right';
                is_moving = true;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            } else if movable.state == 'sliding_right'{
                state = 'sliding_right';
                is_moving = true;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            
            var player = self;
            var can_move = true;
            with (movable) {
                if place_meeting(x,y+1,obj_ice) || is_wheel{
                    var movable_state = state;
                    can_move = true;
                    state = 'sliding_left';
                    occupy_reserved = false;
                    is_moving = true;
                    movement_tracking = max_movement_per_command;
                    
                    //Return player states to stuck
                    if movable_state == 'hold_R'{
                        player.state = 'sliding_right';
                        player.is_moving = true;
                        player.previous_state = 'sliding_left';
                        player.movement_tracking = player.max_movement_per_command;
                        player.occupy_reserved = false;
                    } else {
                        player.state = 'stuck';
                        player.is_moving = false;
                        player.previous_state = 'sliding_left';
                        player.movement_tracking = player.max_movement_per_command;
                        player.occupy_reserved = false;
                    }
                } else if !place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_ladder){
                    can_move = 'false';
                    state = 'falling';
                    is_moving = true;
                    previous_state = 'sliding_left';
                    player.state = 'hold';
                    player.is_moving = false;
                } else {
                    can_move = 'false';
                    state = 'idle';
                    is_moving = false;
                    previous_state = 'sliding_left';
                    movement_tracking = max_movement_per_command;
                    occupy_reserved = false;
                }
            }
            if movable.state == 'idle'{
                state = 'stuck';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
        }       
        // Here we are sliding and we should take away control from the player
        // untill he stops sliding
        // We make sure while sliding, nothing is infront of us so we dont get stuck
        if !place_meeting(x-1,y,obj_solid) && is_moving{
            if state == 'sliding_left'{
                x -= self_speed;
                movement_tracking -= self_speed;
            } 
        }        
        if movement_tracking <= 0 {
            if place_meeting(x-1,y,obj_movable){
                var movable = instance_place(x-1,y,obj_movable);
                if movable.state == 'holding_R'{
                    state = 'sliding_right';
                    is_moving = true;
                    occupy_reserved = false;
                    previous_state = 'sliding_left';
                    movement_tracking = max_movement_per_command;
                    self_speed = original_self_speed;
                }
            } else {
                state = 'idle';
                occupy_reserved = false;
                is_moving = false;
                previous_state = 'sliding_left';
                movement_tracking = max_movement_per_command;
                self_speed = original_self_speed;
            }
            // ONLY give back control when the player stopped sliding
            if !place_meeting(x,y+1,obj_ice) && place_meeting(x,y+1,obj_solid){
                state = 'idle';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            // also, if the player hit something, he should stop
            // except now the player is frozen (stuck) and can no longer push anything
            
            // First check if the object is immovable
            if place_meeting(x-1,y,obj_wall){
                state = 'stuck';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
        }
        break;
        
// ************** HOLDING **************
        case 'hold':
            movement_tracking -= self_speed;
            if movement_tracking <= 0 {
                state = previous_state;
                is_moving = true;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            break;
        case 'hold_L':
            if place_meeting(x-1,y,obj_solid){
                // We only end hold_R if there is no ice under us (if there is ice, then we should keep holding)
                if !place_meeting(x,y+1,obj_ice){
                    state = 'idle';
                    is_moving = false;
                    movement_tracking = max_movement_per_command;
                    occupy_reserved = false;
                }
            } else {
                movement_tracking -= self_speed;
                if movement_tracking <= 0 {
                    state = previous_state;
                    is_moving = true;
                    movement_tracking = max_movement_per_command;
                    occupy_reserved = false;
                }
            }
            break;
        case 'hold_R':
            if place_meeting(x+1,y,obj_solid){
                // We only end hold_R if there is no ice under us (if there is ice, then we should keep holding)
                if !place_meeting(x,y+1,obj_ice){
                    state = 'idle';
                    is_moving = false;
                    movement_tracking = max_movement_per_command;
                    occupy_reserved = false;
                }
            } else {
                movement_tracking -= self_speed;
                if movement_tracking <= 0 {
                    state = previous_state;
                    is_moving = true;
                    movement_tracking = max_movement_per_command;
                    occupy_reserved = false;
                }
            }
            break;
        case 'hold_D':
            // This is a special case for when falling and another object is moving under us
            if !place_meeting(x,y+1,obj_solid){
                state = 'falling';
                is_moving = true;
            }
            break;
// ************** MOVING RIGHT **************
    case 'moving_right':
        // Reserving left
        
        // First make sure nothing is on right
        if !place_meeting(x+1,y,obj_occupy_L){
            // Now check if another object already occupied the space right to us
            if place_meeting(x+1,y,obj_occupy_L){
                state = 'hold_R';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            } else {
                if occupy_reserved == false {
                    occupy_reserved = true;
                    var occupy = instance_create(x+32,y,obj_occupy_R);
                    var occupy_timer = self_speed;
                    var occupy_time_to_destroy = movement_tracking  - self_speed;
                    with (occupy) {
                        time_to_destroy = occupy_time_to_destroy;
                        timer = occupy_timer;
                        
                        if place_meeting(x,y,obj_occupy_L){
                            occupy_free = false;
                        }
                    }
                }
            }   
        } else {
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command;
            occupy_reserved = false;
        }
        
        if !place_meeting(x+1,y,obj_solid) && is_moving {
            x += self_speed;
            movement_tracking -= self_speed;
        } else {
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command;
            occupy_reserved = false;
            self_speed = original_self_speed;
        }
        if movement_tracking <= 0 {
            previous_state = 'moving_right';
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command;
            occupy_reserved = false;
            self_speed = original_self_speed;
        }
        break;
// ************** PUSHING RIGHT **************
    case 'pushing_right': 
        var movable = instance_place(x+1,y,obj_movable);
        var can_move = true;
        var mySpeed = self_speed;
        with (movable) {
            if !place_meeting(x+1,y,obj_solid) && !place_meeting(x+1,y,obj_spring){
                if !place_meeting(x+1,y,obj_occupy_father) {
                    if state != 'moving_right' && state != 'rolling_right' && state != 'falling'{
                        state = 'moving_right';
                        is_moving = true;
                        previous_state = 'idle';
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                        self_speed = mySpeed;
                    }
                } else {
                    can_move = false;
                }
            } else {
                can_move = false;
            }
        }
        if (can_move){
            if !place_meeting(x+1,y,obj_solid) && is_moving {
                x += self_speed;
                movement_tracking -= self_speed;
            }
            if movement_tracking <= 0 {
                previous_state = 'pushing_right';
                state = 'idle';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
        } else {
            previous_state = 'pushing_right';
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command;
            occupy_reserved = false;
        }
        break;
// ************** SLIDING RIGHT **************
    case 'sliding_right':
        // Reserving right
        
        // First make sure nothing is on right
        if !place_meeting(x+1,y,obj_solid){
            // Now check if another object already occupied the space right to us
            if place_meeting(x+1,y,obj_occupy_L){
                state = 'hold_R';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            } else {
            // If the space right to us is free, occupy it
                if occupy_reserved == false {
                    occupy_reserved = true;
                    var occupy = instance_create(x+32,y,obj_occupy_R);
                    var occupy_timer = self_speed;
                    var occupy_time_to_destroy = movement_tracking - self_speed;
                    var occupy_free = true;
                    with (occupy) {
                        time_to_destroy = occupy_time_to_destroy;
                        timer = occupy_timer;
                        
                        if place_meeting(x,y,obj_occupy_L){
                            occupy_free = false;
                        }
                    }
                }
            }  
        }
        
        if place_meeting(x+1,y,obj_movable) {
            var movable = instance_place(x+1,y,obj_movable);
            if movable.state == 'idle' || movable.state == 'stuck'{
                state = 'stuck';
                is_moving = false;
                previous_state = 'sliding_right';
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            } else if movable.state == 'hold' {
                state = 'hold';
                is_moving = false;
                previous_state = 'sliding_right';
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            } else if movable.state == 'hold_L'{
                state = 'sliding_left';
                is_moving = true;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            } else if movable.state == 'sliding_left'{
                state = 'sliding_left';
                is_moving = true;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            var player = self;
            var can_move = true;
            with (movable) {
                if place_meeting(x,y+1,obj_ice) || is_wheel{
                    var movable_state = state;
                    can_move = true;
                    state = 'sliding_right';
                    occupy_reserved = false;
                    is_moving = true;
                    movement_tracking = max_movement_per_command;
                    
                    //Return player states to stuck
                    if movable_state == 'hold_L'{
                        player.state = 'sliding_left';
                        player.is_moving = true;
                        player.previous_state = 'sliding_right';
                        player.movement_tracking = player.max_movement_per_command;
                        player.occupy_reserved = false;
                    } else {
                        player.state = 'stuck';
                        player.is_moving = false;
                        player.previous_state = 'sliding_right';
                        player.movement_tracking = player.max_movement_per_command;
                        player.occupy_reserved = false;
                    }
                } else if !place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_ladder){
                    can_move = 'false';
                    state = 'falling';
                    is_moving = true;
                    previous_state = 'sliding_right';
                    player.state = 'hold';
                    player.is_moving = false;
                } else {
                    can_move = 'false';
                    state = 'idle';
                    is_moving = false;
                    previous_state = 'sliding_right';
                    movement_tracking = max_movement_per_command;
                    occupy_reserved = false;
                }
            }
            if movable.state == 'idle'{
                state = 'stuck';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
        }       
        if !place_meeting(x+1,y,obj_solid) && is_moving{
            if state == 'sliding_right'{
                x += self_speed;
                movement_tracking -= self_speed;
            }
        }        
        if movement_tracking <= 0 {
            if place_meeting(x+1,y,obj_movable){
                var movable = instance_place(x+1,y,obj_movable);
                if movable.state == 'holding_L'{
                    state = 'sliding_left';
                    is_moving = true;
                    occupy_reserved = false;
                    previous_state = 'sliding_right';
                    movement_tracking = max_movement_per_command;
                    self_speed = original_self_speed;
                }
            } else {
                state = 'idle';
                is_moving = false;
                occupy_reserved = false;
                previous_state = 'sliding_right';
                movement_tracking = max_movement_per_command
                self_speed = original_self_speed;
            }
            if !place_meeting(x,y+1,obj_ice) && place_meeting(x,y+1,obj_solid){
                state = 'idle';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            if place_meeting(x+1,y,obj_wall){
                state = 'stuck';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
        }
        break;
// ************** UP **************
    case 'climbing_up':
        y -= self_speed;
        movement_tracking -= self_speed;
        if movement_tracking <= 0 {
            previous_state = 'climbing_up';
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command
        }
        break;
// ************** DOWN **************
    case 'climbing_down':
        y += self_speed;
        movement_tracking -= self_speed;
        if movement_tracking <= 0 {
            previous_state = 'climbing_down';
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command
        }
        break;
        
// ************** GRAVITY **************
    case 'falling':
        // NOTE : Check for when falling and another object is rolling or lsiding !!!!!!!!
        y += self_speed;
        movement_tracking -= self_speed;
        if movement_tracking <= 0 {
            state = 'idle';
            is_moving = false;
            movement_tracking = max_movement_per_command
            if place_meeting(x,y+1,obj_movable){
                var movable = instance_place(x,y+1,obj_movable);
                if movable.is_moving == true {
                    state = 'hold_D';
                    is_moving = false;
                }
            }
        }
        break;
}
