///scr_wheel_rules();

// This rule will apply to wheel so they can keep rolling when we push them once.
switch(state) {
    case 'idle':
        // In our idle state, if we're not falling, then keep rolling
        if !falling && rolling{
            if !place_meeting(x,y+1,obj_rock01){
                state = previous_state;
                is_moving = true;
            } else {
                state = 'idle';
                is_moving = false;
            }
        }
        // If we fell, once we hit the ground we should turn falling to false and continue rolling
        if place_meeting(x,y+1,obj_solid){
            if falling {
                falling = false;
                if previous_state == 'rolling_left' || previous_state == 'rolling_right'{
                    rolling = true;
                }
            }
        }
        break;
    // fixing sliding on ice
    case 'sliding_left':
        if !place_meeting(x-1,y,obj_occupy_R){
            state = 'rolling_left';
            is_moving = true;
            rolling = true;
        }
        break;
    case 'sliding_right':
        if !place_meeting(x+1,y,obj_occupy_L){
            state = 'rolling_right';
            is_moving = true;
            rolling = true;
        }
        break;
    case 'moving_left':
        if !place_meeting(x-1,y,obj_occupy_R){
            state = 'rolling_left';
            is_moving = true;
            rolling = true;
        }
        break;
    case 'moving_right':
        if !place_meeting(x+1,y,obj_occupy_L){
            state = 'rolling_right';
            is_moving = true;
            rolling = true;
        }
        break;
    case 'rolling_left':
        // Reserving left
        
        // First make sure nothing is on left
        if !place_meeting(x-1,y,obj_solid){
            // Now check if another object already occpies the space left to us
            if place_meeting(x-1,y,obj_occupy_R){
                state = 'hold_L';
                rolling = false;
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = true;
            }  else {
            // If the space left to us is free, occupy it
                if occupy_reserved == false {
                    occupy_reserved = true;
                    var occupy = instance_create(x-32,y,obj_occupy_L);
                    var occupy_timer = self_speed;
                    var occupy_time_to_destroy = movement_tracking - self_speed;
                    var occupy_free = true;
                    with (occupy) {
                        time_to_destroy = occupy_time_to_destroy;
                        timer = occupy_timer;
                        
                        if place_meeting(x,y,obj_occupy_R){
                            occupy_free = false;
                        }
                    }
                }
            }
        }
        
        if !place_meeting(x-1,y,obj_solid) && is_moving{
            x -= self_speed;
            movement_tracking -= self_speed;
        }
        if movement_tracking <= 0 {
            // Exception for fraction grounds
            if place_meeting(x,y+1,obj_rock01){
                previous_state = 'rolling_left';
                state = 'idle';
                occupy_reserved = false;
                is_moving = false;
                movement_tracking = max_movement_per_command;
                rolling = false;
            } else {
                previous_state = 'rolling_left';
                state = 'rolling_left';
                occupy_reserved = false;
                is_moving = true;
                movement_tracking = max_movement_per_command;
            }
        }
        // Apply collision
        if place_meeting(x-1,y,obj_solid){
            rolling = false;
            state = 'idle';
            is_moving = false;
            previous_state = 'rolling_left';
            movement_tracking = max_movement_per_command 
            occupy_reserved = false;
            // Here we apply rolling to another wheel if we collide with it
            if place_meeting(x-1,y,obj_movable){
                // To control taffic, we apply hold to ourselves if anothing object is flaling next to us
                var movable = instance_place(x-1,y,obj_movable);
                if movable.state == 'falling'{
                    state = 'hold';
                    is_moving = false;
                    previous_state = 'rolling_left';
                    occupy_reserved = false;
                    movement_tracking = max_movement_per_command + (max_movement_per_command/8);
                }
                
                if place_meeting(x-1,y,obj_wheel){
                    var wheel = instance_place(x-1,y,obj_wheel);
                    var wheel_current_state = wheel.state;
                    with (wheel){
                        if state != 'falling'{
                            state = 'rolling_left';
                            is_moving = true;
                            movement_tracking = max_movement_per_command;
                            occupy_reserved = false;
                        }
                    }
                    if wheel_current_state == 'rolling_right'{
                        state = 'rolling_right';
                        is_moving = true;
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                    } else if wheel_current_state == 'hold_R'{
                        state = 'rolling_right';
                        is_moving = true;
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                    }
                }
            }
            // Apply ice rules
            if place_meeting(x,y+1,obj_ice){
                if place_meeting(x-1,y,obj_movable){
                    var movable = instance_place(x-1,y,obj_movable);
                    if movable.previous_state == 'sliding_right'{
                        state = 'rolling_right';
                        is_moving = true;
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                    }
                    with (movable) {
                        if place_meeting(x,y+1,obj_ice){
                            state = 'sliding_left';
                            is_moving = true;
                            movement_tracking = max_movement_per_command;
                            occupy_reserved = false;
                        }
                    }
                }
            }
        }
        // Apply gravity
        if !place_meeting(x,y+1,obj_solid){
            if !place_meeting(x,y+1,obj_ladder){
                rolling = false;
                state = 'falling';
                is_moving = true;
                previous_state = 'rolling_left';
                movement_tracking = max_movement_per_command
                falling = true;
            }
        }
        break;
    case 'rolling_right':
        // Reserving left
        
        // First make sure nothing is on right
        if !place_meeting(x+1,y,obj_solid){
            // Now check if another object already occupied the space right to us
            if place_meeting(x+1,y,obj_occupy_L){
                state = 'hold_R';
                rolling = false;
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = true;
            } else {
                if occupy_reserved == false {
                    occupy_reserved = true;
                    var occupy = instance_create(x+32,y,obj_occupy_R);
                    var occupy_timer = self_speed;
                    var occupy_time_to_destroy = movement_tracking  - self_speed;
                    with (occupy) {
                        time_to_destroy = occupy_time_to_destroy;
                        timer = occupy_timer;
                        
                        if place_meeting(x,y,obj_occupy_L){
                            occupy_free = false;
                        }
                    }
                } 
            }               
        }
        
        if !place_meeting(x+1,y,obj_solid) && is_moving {
            x += self_speed;
            movement_tracking -= self_speed;
        }
        if movement_tracking <= 0 {
            // Exception for fraction grounds
            if place_meeting(x,y+1,obj_rock01){
                previous_state = 'rolling_right';
                state = 'idle';
                occupy_reserved = false;
                is_moving = false;
                movement_tracking = max_movement_per_command;
                rolling = false;
            } else {
                previous_state = 'rolling_right';
                state = 'rolling_right';
                occupy_reserved = false;
                is_moving = true;
                movement_tracking = max_movement_per_command;
            }
        }
        
        // Apply collision
        if place_meeting(x+1,y,obj_solid){
            rolling = false;
            state = 'idle';
            is_moving = false;
            previous_state = 'rolling_right';
            movement_tracking = max_movement_per_command
            occupy_reserved = false;
            // Here we apply rolling to another wheel if we collide with it
            if place_meeting(x+1,y,obj_movable){
            
                // To control taffic, we apply hold to ourselves if anothing object is flaling next to us
                var movable = instance_place(x+1,y,obj_movable);
                if movable.state == 'falling'{
                    state = 'hold';
                    is_moving = false;
                    previous_state = 'rolling_right';
                    occupy_reserved = false;
                    movement_tracking = max_movement_per_command + (max_movement_per_command/8);
                }
                
                if place_meeting(x+1,y,obj_wheel){
                    var wheel = instance_place(x+1,y,obj_wheel);
                    var wheel_current_state = wheel.state;
                    with (wheel){
                        if state != 'falling'{
                            state = 'rolling_right';
                            is_moving = true;
                            movement_tracking = max_movement_per_command;
                            occupy_reserved = false;
                        }
                    }
                    if wheel_current_state == 'rolling_left'{
                        state = 'rolling_left';
                        is_moving = true;
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                    } else if wheel_current_state == 'hold_L'{
                        state = 'rolling_left';
                        is_moving = true;
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                    }
                }            
            }
            // Apply ice rules
            if place_meeting(x,y+1,obj_ice){
                if place_meeting(x+1,y,obj_movable){
                    var movable = instance_place(x+1,y,obj_movable);
                    if movable.previous_state == 'sliding_left'{
                        state = 'rolling_left';
                        is_moving = true;
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                    }
                    with (movable) {
                        if place_meeting(x,y+1,obj_ice){
                            state = 'sliding_right';
                            is_moving = true;
                            movement_tracking = max_movement_per_command;
                            occupy_reserved = false;
                        }
                    }
                }
            }
        }
        // Apply gravity
        if !place_meeting(x,y+1,obj_solid){
            if !place_meeting(x,y+1,obj_ladder){
                rolling = false;
                state = 'falling';
                is_moving = true;
                previous_state = 'rolling_right';
                movement_tracking = max_movement_per_command
                falling = true;
            }
        }
        break;
}
