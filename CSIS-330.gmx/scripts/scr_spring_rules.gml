/// scr_spring_rules

// SPRING IS RIGHT
if place_meeting(x+1,y,obj_spring){
    if previous_state == 'rolling_right' {
        var spring = instance_place(x+1,y,obj_spring);
        state = 'rolling_left';
        is_moving = true;
        movement_tracking = max_movement_per_command;
        occupy_reserved = false;
        with(spring){
            sprite_index = spr_spring01_R;
            image_index = 0;
            image_speed = 2;
        }
    } else if (previous_state == 'sliding_right' || previous_state == 'moving_right') && (place_meeting(x,y+1,obj_ice) || is_frozen){
        var spring = instance_place(x+1,y,obj_spring);
        state = 'sliding_left';
        is_moving = true;
        movement_tracking = max_movement_per_command;
        occupy_reserved = false;
        with(spring){
            sprite_index = spr_spring01_R;
            image_index = 0;
            image_speed = 2;
        }
    }
// SPRING IS LEFT
} else if place_meeting(x-1,y,obj_spring){
    if previous_state == 'rolling_left' {
        var spring = instance_place(x-1,y,obj_spring);
        state = 'rolling_right';
        is_moving = true;
        movement_tracking = max_movement_per_command;
        occupy_reserved = false;
        with(spring){
            sprite_index = spr_spring01_L;
            image_index = 0;
            image_speed = 2;
        }
    // This will apply for sliding objects
    } else if (previous_state == 'sliding_left' || previous_state == 'moving_left') && (place_meeting(x,y+1,obj_ice) || is_frozen){
        var spring = instance_place(x-1,y,obj_spring);
        state = 'sliding_right';
        is_moving = true;
        movement_tracking = max_movement_per_command;
        occupy_reserved = false;
        with(spring){
            sprite_index = spr_spring01_L;
            image_index = 0;
            image_speed = 2;
        }
    }
}
