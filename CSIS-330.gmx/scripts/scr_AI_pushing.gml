///scr_AI_pushing
var mov = argument0
if state == 'moving_right' && is_frozen == false {
    if !place_meeting(x+1,y,obj_wall){
        if !place_meeting(x+1,y,obj_immovable){
            if place_meeting(x+1,y,obj_movable){
                var movable = instance_place(x+1,y,obj_movable);
                if movable.is_moving == false{
                    // Make sure the space right of us is not occupies
                    if !place_meeting(x+1,y,obj_occupy_L){
                        state = 'moving_right';  
                        is_moving = true; 
                    }
                }
            } else {
                if !place_meeting(x+1,y,obj_occupy_L){
                    state = 'moving_right';
                    is_moving = true;
                }
            }
        }
    }
    if place_meeting(x+1,y,obj_movable) {
        var movable = instance_place(x+1,y,obj_movable);
        if movable.is_moving == false{
            if place_meeting(movable.x,movable.y+1,obj_rock01) {
                if movable.is_frozen == true || movable.is_wheel {
                    state = 'pushing_right';
                    is_moving = true;
                } else {
                    state = 'idle';
                    is_moving = false;
                }
            } else {
                state = 'pushing_right';
                is_moving = true;
            }
        }
    }
}
if state == 'moving_left' && is_frozen == false {
    // First, we make sure that one pixel (-1) left of us is not a wall
    if !place_meeting(x-1,y,obj_wall){
        if !place_meeting(x-1,y,obj_immovable){
            // First lets make sure no movable is blocking our path (in case the movable is falling or moving towards us)
            if place_meeting(x-1,y,obj_movable){
                var movable = instance_place(x-1,y,obj_movable);
                if movable.is_moving == false{
                    // Make sure the space left of us is not occupies
                    if !place_meeting(x-1,y,obj_occupy_R){
                        state = 'moving_left';
                        is_moving = true;
                    }
                }
            } else {
                if !place_meeting(x-1,y,obj_occupy_R){
                    state = 'moving_left';
                    is_moving = true;
                }
            }
        }
    }
    
    // If another object is left of us, we are either able to push it or not
    if place_meeting(x-1,y,obj_movable) {
        // We should only be allowed to push something if it was not moving
        var movable = instance_place(x-1,y,obj_movable);
        if movable.is_moving == false {
            // Exceptions for friction gounrd (frozen and wheel objects still can be pushed)
            if place_meeting(movable.x,movable.y+1,obj_rock01) {
                if movable.is_frozen == true || movable.is_wheel{
                    state = 'pushing_left';
                    is_moving = true;
                } else {
                    state = 'idle';
                    is_moving = false;
                }
            } else {
                state = 'pushing_left';
                is_moving = true;
            }
        }
    }
}
