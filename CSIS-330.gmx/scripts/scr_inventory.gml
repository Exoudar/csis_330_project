///scr_inventory()
if place_meeting(x,y,obj_items){
    var item = instance_place(x,y,obj_items);
    inventory[0] = item.power_type;
    sprite_for_inventory = item.sprite_index;
    collected = true;
    
    with(item){
        instance_destroy();
    }
}
