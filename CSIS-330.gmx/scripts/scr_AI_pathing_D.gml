///scr_AI_pathing_D
// CLIMBING down
if is_climbingDown == true && is_climbingUp == false && facing == 'down' {
    if place_meeting(x,y+1,obj_player){
        var move = nextMove;
        with(grandFather){
            state = move;
            scr_AI_pushing(move);
            is_moving = true;
            finding_player = true;
            scr_mapping_path();
        }
        with (obj_AI_path){
            instance_destroy();
        }
        exit;
    }
    // First check if we reached this place before
    if place_meeting(x,y,obj_path_pos){
        var pos = instance_place(x,y,obj_path_pos);
        with (pos){
            instance_destroy();
        }
    } else {
        instance_destroy();
        exit;
    }
    if !place_meeting(x,y+1,obj_player) && !place_meeting(x,y+1,obj_wall) && place_meeting(x,y+1,obj_ladder){
        if place_meeting(x,y,obj_player){
            var move = nextMove;
            with(grandFather){
                state = move;
                scr_AI_pushing(move);
                is_moving = true;
                finding_player = true;
                scr_mapping_path();
            }
            with (obj_AI_path){
                instance_destroy();
            }
            exit;
        }
        y += 32;
        
        if place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_player){
            // Testing to go left
            if !place_meeting(x-1,y,obj_wall){
                var path = instance_create(x-32,y,obj_AI_path_L);
                path.nextMove = nextMove;
                path.facing = "left";
                path.grandFather = grandFather;
                if place_meeting(x,y,obj_path_pos){
                    var pos = instance_place(x,y,obj_path_pos);
                    with (pos){
                        instance_destroy();
                    }
                } else {
                    instance_destroy();
                    exit;
                }
            }
            // Testing to go right
            if !place_meeting(x+1,y,obj_wall){
                var path = instance_create(x+32,y,obj_AI_path_R);
                path.nextMove = nextMove;
                path.facing = "right";
                path.grandFather = grandFather;
                if place_meeting(x,y,obj_path_pos){
                    var pos = instance_place(x,y,obj_path_pos);
                    with (pos){
                        instance_destroy();
                    }
                } else {
                    instance_destroy();
                    exit;
                }
            } 
            instance_destroy();
        } else {
            // Testing to go right
            if !place_meeting(x+1,y,obj_wall){
                var path = instance_create(x+32,y,obj_AI_path_R);
                path.nextMove = nextMove;
                path.facing = "right";
                path.grandFather = grandFather;
            }
            // Testing to go left
            if !place_meeting(x-1,y,obj_wall){
                var path = instance_create(x-32,y,obj_AI_path_L);
                path.nextMove = nextMove;
                path.facing = "left";
                path.grandFather = grandFather;
            }
            if place_meeting(x,y+1,obj_player){
                var move = nextMove;
                with(grandFather){
                    state = move;
                    scr_AI_pushing(move);
                    is_moving = true;
                    finding_player = true;
                    scr_mapping_path();
                }
                with (obj_AI_path){
                    instance_destroy();
                }
                exit;
            }
        }
    } else {
        // Testing to go left
        if !place_meeting(x-1,y,obj_wall){
            var path = instance_create(x-32,y,obj_AI_path_L);
            path.nextMove = nextMove;
            path.facing = "left";
            path.grandFather = grandFather;
        }
        // Testing to go right
        if !place_meeting(x+1,y,obj_wall){
            var path = instance_create(x+32,y,obj_AI_path_R);
            path.nextMove = nextMove;
            path.facing = "right";
            path.grandFather = grandFather;
        }
    }
}
