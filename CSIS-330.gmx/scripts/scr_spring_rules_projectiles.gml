/// scr_spring_rules_projectiles()

// SPRING IS RIGHT
if place_meeting(x+1,y,obj_spring) && facing == 'right'{
   facing = 'left';
   var spring = instance_place(x+1,y,obj_spring);
   with(spring){
        sprite_index = spr_spring01_R;
        image_index = 0;
        image_speed = 2;
    }
// SPRING IS LEFT
} else if place_meeting(x-1,y,obj_spring) && facing == 'left'{
    facing = 'right';
    var spring = instance_place(x+1,y,obj_spring);
    with(spring){
        sprite_index = spr_spring01_R;
        image_index = 0;
        image_speed = 2;
    }
}
