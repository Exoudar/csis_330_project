/// scr_AI_pathing_R
// MOVING RIGHT

if facing == 'right' && !is_falling && !is_climbingUp && !is_climbingDown {
    // First check if we reached this place before
    if place_meeting(x,y,obj_path_pos){
        var pos = instance_place(x,y,obj_path_pos);
        with (pos){
            instance_destroy();
        }
    } else {
        instance_destroy();
        exit;
    }
    if (place_meeting(x,y+1,obj_solid) ||  place_meeting(x,y+1,obj_ladder)) && !place_meeting(x,y+1,obj_player){     
        // Testing to go down when a ladder exists
        if place_meeting(x,y+1,obj_ladder) && !place_meeting(x,y+1,obj_wall){
            var path = instance_create(x,y+32,obj_AI_path_D);
            path.nextMove = nextMove;
            path.facing = "down";
            path.is_climbingDown = true;
            path.grandFather = grandFather
        }
        
        // Testing to go up when a ladder exists
        if place_meeting(x,y,obj_ladder) && !place_meeting(x,y-1,obj_wall){
            var path = instance_create(x,y-32,obj_AI_path_U);
            path.nextMove = nextMove;
            path.facing = "up";
            path.is_climbingUp = true;
            path.grandFather = grandFather
        }
        if !place_meeting(x+1,y,obj_wall){
            x += 32;
        }
    } else {
        if place_meeting(x,y,obj_player){
            var move = nextMove;
            with(grandFather){
                state = move;
                scr_AI_pushing(move);
                is_moving = true;
                finding_player = true;
                scr_mapping_path();
            }
            with (obj_AI_path){
                instance_destroy();
            }
            exit;
        }
        y += 32;        
        if place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_player){
            if place_meeting(x,y,obj_path_pos){
                var pos = instance_place(x,y,obj_path_pos);
                with (pos){
                    instance_destroy();
                }
            } else {
                instance_destroy();
                exit;
            }
            if !place_meeting(x+1,y,obj_wall){
                var path = instance_create(x+32,y,obj_AI_path_R);
                path.nextMove = nextMove;
                path.facing = "right";
                path.grandFather = grandFather;
            }
            if !place_meeting(x-1,y,obj_wall){
                var path = instance_create(x-32,y,obj_AI_path_L);
                path.nextMove = nextMove;
                path.facing = "left";
                path.grandFather = grandFather;
            }
            instance_destroy();
        }
    }
    if place_meeting(x,y,obj_player){
        var move = nextMove;
        with(grandFather){
            state = move;
            scr_AI_pushing(move);
            is_moving = true;
            finding_player = true;
            scr_mapping_path();
        }
        with (obj_AI_path){
            instance_destroy();
        }
        exit;
    }
}
