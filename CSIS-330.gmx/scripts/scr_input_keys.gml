///scr_input_keys();

// setting shortcuts for our movement keys
var left = keyboard_check(vk_left);
var right = keyboard_check(vk_right);
var up = keyboard_check(vk_up);
var down = keyboard_check(vk_down);

// ***LEFT COMMAND***

// First we make sure we are in idle state (not moving)
if (left) && state == 'idle' && is_frozen == false {
    // First, we make sure that one pixel (-1) left of us is not a wall
    if !place_meeting(x-1,y,obj_wall){
        if !place_meeting(x-1,y,obj_immovable){
            // First lets make sure no movable is blocking our path (in case the movable is falling or moving towards us)
            if place_meeting(x-1,y,obj_movable){
                var movable = instance_place(x-1,y,obj_movable);
                if movable.is_moving == false{
                    // Make sure the space left of us is not occupies
                    if !place_meeting(x-1,y,obj_occupy_R){
                        state = 'moving_left';
                        is_moving = true;
                    }
                }
            } else {
                if !place_meeting(x-1,y,obj_occupy_R){
                    state = 'moving_left';
                    is_moving = true;
                }
            }
        }
    }
    
    // If another object is left of us, we are either able to push it or not
    if place_meeting(x-1,y,obj_movable) {
        // We should only be allowed to push something if it was not moving
        var movable = instance_place(x-1,y,obj_movable);
        if movable.is_moving == false {
            // Exceptions for friction gounrd (frozen and wheel objects still can be pushed)
            if place_meeting(movable.x,movable.y+1,obj_rock01) {
                if movable.is_frozen == true || movable.is_wheel{
                    state = 'pushing_left';
                    is_moving = true;
                } else {
                    state = 'idle';
                    is_moving = false;
                }
            } else {
                state = 'pushing_left';
                is_moving = true;
            }
        }
    }
}

// RIGHT COMMAND

// First we make sure we are in idle state (not moving)
if (right) && state == 'idle' && is_frozen == false {
    if !place_meeting(x+1,y,obj_wall){
        if !place_meeting(x+1,y,obj_immovable){
            if place_meeting(x+1,y,obj_movable){
                var movable = instance_place(x+1,y,obj_movable);
                if movable.is_moving == false{
                    // Make sure the space right of us is not occupies
                    if !place_meeting(x+1,y,obj_occupy_L){
                        state = 'moving_right';  
                        is_moving = true; 
                    }
                }
            } else {
                if !place_meeting(x+1,y,obj_occupy_L){
                    state = 'moving_right';
                    is_moving = true;
                }
            }
        }
    }
    if place_meeting(x+1,y,obj_movable) {
        var movable = instance_place(x+1,y,obj_movable);
        if movable.is_moving == false{
            if place_meeting(movable.x,movable.y+1,obj_rock01) {
                if movable.is_frozen == true || movable.is_wheel {
                    state = 'pushing_right';
                    is_moving = true;
                } else {
                    state = 'idle';
                    is_moving = false;
                }
            } else {
                state = 'pushing_right';
                is_moving = true;
            }
        }
    }
}

/// UP COMMAND
if (up) && (state == 'idle' || state == 'stuck') && is_frozen == false  {
    // We check if we are inside a ladder so we can climb it.
    // And we also make sure we are idle (not sliding, frozen, stunned,... etc)
    if place_meeting(x,y,obj_ladder){
        // We make sure nothing is blocking our path towards up
        if !place_meeting(x,y-1,obj_solid){
            state = 'climbing_up';
            is_moving = true;
        }
    }
}

/// DOWN COMMAND
if (down) && (state == 'idle'  || state == 'stuck') && is_frozen == false  {
    // We check if we are inside a ladder so we can climb it.
    // And we also make sure we are idle (not sliding, frozen, stunned,... etc)
    // NOTE: we want to also make sure a ladder is under us. Hence, the y+1
    if place_meeting(x,y,obj_ladder) || place_meeting(x,y+1,obj_ladder){
        // We make sure nothing is blocking our path towards up
        if !place_meeting(x,y+1,obj_solid){
            state = 'climbing_down';
            is_moving = true;
        }
    }
}
