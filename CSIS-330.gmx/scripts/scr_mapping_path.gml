/// scr_mapping_path()
grid_width = room_width div 32;
grid_height = room_height div 32;


for (var i = 0; i < grid_width; i ++){
    for (var j = 0; j < grid_height; j ++) {
        if !place_meeting(i*32,j*32,obj_path_pos){
            instance_create(i*32,j*32,obj_path_pos);
        }
    }
}
