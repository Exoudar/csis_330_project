///scr_magnet_rules()

if place_meeting(x+1,y,obj_metal)  && (state == 'idle' || state == 'magnitized' || state == 'stuck'){
    var metal = instance_place(x+1,y,obj_metal);
    // First make sure it is a metal
    if metal.is_metal == true{
        // If we are facing right
        if side == 'right'{
            if (metal.state == 'idle' || metal.state == 'magnitized'  || state == 'stuck'){
                // Magnitizing
                if metal.side == 'right'{
                    if state != 'magnitized' {
                        // play sound here ***
                        audio_play_sound(snd_magnetClick,0,0);
                    }
                    state = 'magnitized';
                    is_magnitized = true;
                    metal.state = 'magnitized';
                    metal.is_magnitized = true;
                } else if metal.side == 'left'{
                // Repelling
                    // For us
                    if place_meeting(x,y+1,obj_rock01){
                        if is_frozen == true {
                            state = 'moving_left';
                            is_moving = true;
                            movement_tracking = max_movement_per_command;
                            occupy_reserved = false;
                            // play sound here ***
                            audio_play_sound(snd_magnetRepel,0,0);
                        }
                    } else {
                        if place_meeting(x,y+1,obj_ice) || is_frozen {
                            state = 'sliding_left';
                        } else {
                            state = 'moving_left';
                        }
                        is_moving = true;
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                        // play sound here ***
                        audio_play_sound(snd_magnetRepel,0,0);
                    }
                    // For the other metal (will go the opposite direction)
                    with (metal){
                        if place_meeting(x,y+1,obj_rock01){
                            if is_frozen == true {
                                state = 'moving_right';
                                is_moving = true;
                                movement_tracking = max_movement_per_command;
                                occupy_reserved = false;
                            }
                        } else {
                            if place_meeting(x,y+1,obj_ice) || is_frozen {
                                state = 'sliding_right';
                            } else {
                                state = 'moving_right';
                            }
                            is_moving = true;
                            movement_tracking = max_movement_per_command;
                            occupy_reserved = false;
                        }
                    }
                } else {
                    // For metals in general, metalize them
                    state = 'magnitized';
                    is_magnitized = true;
                    metal.state = 'magnitized';
                    metal.is_magnitized = true;
                }
            }
        } else if side == 'left'{
            if (metal.state == 'idle' || metal.state == 'magnitized'  || state == 'stuck'){
                // Magnitizing
                if metal.side == 'left'{
                    if state != 'magnitized' {
                        // play sound here ***
                        audio_play_sound(snd_magnetClick,0,0);
                    }
                    state = 'magnitized';
                    is_magnitized = true;
                    metal.state = 'magnitized';
                    metal.is_magnitized = true;
                }
            }
        }  else if side == 'none' {
            if place_meeting(x+1,y,obj_magnet){
                var magnet = instance_place(x+1,y,obj_magnet);
                if magnet.side == 'left'{
                    state = 'magnitized';
                    is_magnitized = true;
                    magnet.state = 'magnitized';
                    magnet.is_magnitized = true;
                }
            }
        }
    }
} else if place_meeting(x-1,y,obj_metal) && (state == 'idle' || state == 'magnitized' || state == 'stuck'){
    var metal = instance_place(x-1,y,obj_metal);
    if metal.is_metal == true{
        if side == 'left'{
            if metal.state == 'idle' || metal.state == 'magnitized'{
                // Magnitizing
                if metal.side == 'left'{
                    if state != 'magnitized' {
                        // play sound here ***
                        audio_play_sound(snd_magnetClick,0,0);
                    }
                    state = 'magnitized';
                    is_magnitized = true;
                    metal.state = 'magnitized';
                    metal.is_magnitized = true;        
                } else if metal.side == 'right' {
                // Repelling
                    // For us
                    if place_meeting(x,y+1,obj_rock01){
                        if is_frozen == true {
                            state = 'moving_right';
                            is_moving = true;
                            movement_tracking = max_movement_per_command;
                            occupy_reserved = false;
                            // play sound here ***
                            audio_play_sound(snd_magnetRepel,0,0);
                        }
                    } else {
                        if place_meeting(x,y+1,obj_ice) || is_frozen {
                            state = 'sliding_right';
                        } else {
                            state = 'moving_right';
                        }
                        is_moving = true;
                        movement_tracking = max_movement_per_command;
                        occupy_reserved = false;
                        // play sound here ***
                        audio_play_sound(snd_magnetRepel,0,0);
                    }
                    
                    // For the other metal (will go the opposite direction)
                    with (metal){
                        if place_meeting(x,y+1,obj_rock01){
                            if is_frozen == true {
                                state = 'moving_left';
                                is_moving = true;
                                movement_tracking = max_movement_per_command;
                                occupy_reserved = false;
                            }
                        } else {
                            if place_meeting(x,y+1,obj_ice) || is_frozen {
                                state = 'sliding_left';
                            } else {
                                state = 'moving_left';
                            }
                            is_moving = true;
                            movement_tracking = max_movement_per_command;
                            occupy_reserved = false;
                        }
                    }
                } else {
                    // For metals in general, metalize them
                    state = 'magnitized';
                    is_magnitized = true;
                    metal.state = 'magnitized';
                    metal.is_magnitized = true;
                }
            }
        } else if side == 'right'{
            if (metal.state == 'idle'|| metal.state == 'magnitized' || state == 'stuck'){
                // Magnitizing
                if metal.side == 'right'{
                    if state != 'magnitized' {
                        // play sound here ***
                        audio_play_sound(snd_magnetClick,0,0);
                    }
                    state = 'magnitized';
                    is_magnitized = true;
                    metal.state = 'magnitized';
                    metal.is_magnitized = true;
                }
            }
        } else if side == 'none' {
            if place_meeting(x-1,y,obj_magnet){
                var magnet = instance_place(x-1,y,obj_magnet);
                if magnet.side == 'right'{
                    state = 'magnitized';
                    is_magnitized = true;
                    magnet.state = 'magnitized';
                    magnet.is_magnitized = true;
                }
            }
        }
    }
}
// Fixing gravity after 2 metals stuck together
if state == 'magnitized' && !place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_ladder) {
    if place_meeting(x+1,y,obj_metal){
        var Rmetal = instance_place(x+1,y,obj_metal);
        var willFall = false;
        with (Rmetal){
            if !place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_ladder){
                willFall = true;
            }
        }
        if willFall {
            state = 'falling';
            Rmetal.state = 'falling';
        }
    } else if place_meeting(x-1,y,obj_metal){
        var Lmetal = instance_place(x-1,y,obj_metal);
        var willFall = false;
        with (Lmetal){
            if !place_meeting(x,y+1,obj_solid) && !place_meeting(x,y+1,obj_ladder){
                willFall = true;
            }
        }
        if willFall {
            state = 'falling';
            Rmetal.state = 'falling';
        }
    }
}
