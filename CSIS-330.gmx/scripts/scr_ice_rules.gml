///scr_ice_rules(direction);
var direction_ = argument0
// If there is an ice below us, then we go to sliding state
if (place_meeting(x,y+1,obj_ice) || is_frozen == true || is_onIce == true) && state == 'idle' && !place_meeting(x,y+1,obj_rock01){
    // We will slide in the direction based on our previous state
    switch (previous_state){
        // LEFT SIDE
        case 'sliding_left':
            if !place_meeting(x-1,y,obj_occupy_R){
                state = 'sliding_left';
                is_moving = true;
                previous_state = 'sliding_left';
                movement_tracking = max_movement_per_command
            } else {
                state = 'hold_L';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            break;
        case 'moving_left':
            if !place_meeting(x-1,y,obj_occupy_R){
                state = 'sliding_left';
                is_moving = true;
                previous_state = 'moving_left';
                movement_tracking = max_movement_per_command
            } else {
                state = 'hold_L';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
                break;
        case 'pushing_left':
            if !place_meeting(x-1,y,obj_occupy_R){
                state = 'sliding_left';
                is_moving = true;
                previous_state = 'pushing_left';
                movement_tracking = max_movement_per_command
            } else {
                state = 'hold_L';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            break;
        // RIGHT SIDE
        case 'sliding_right':
            if !place_meeting(x+1,y,obj_occupy_L){
                state = 'sliding_right';
                is_moving = true;
                previous_state = 'sliding_right';
                movement_tracking = max_movement_per_command
            } else {
                state = 'hold_R';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            break;
        case 'moving_right':
            if !place_meeting(x+1,y,obj_occupy_L){
                state = 'sliding_right';
                is_moving = true;
                previous_state = 'moving_right';
                movement_tracking = max_movement_per_command
            } else {
                state = 'hold_R';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
                break;
        case 'pushing_right':
            if !place_meeting(x+1,y,obj_occupy_L){
                state = 'sliding_right';
                is_moving = true;
                previous_state = 'pushing_right';
                movement_tracking = max_movement_per_command
            } else {
                state = 'hold_R';
                is_moving = false;
                movement_tracking = max_movement_per_command;
                occupy_reserved = false;
            }
            break;
    }
}
